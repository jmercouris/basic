!__________________________________________________________
!
! Program Written by John Mercouris
!__________________________________________________________
!
! February 14, 2007
!__________________________________________________________
!
! This program manipulates strings in various ways using a 
! menu driven console. When the program starts, it displays
! a menu and lets the user choose a mini application,
! ( a subprogram), such as to print a inputted sentence
! backwards, count words, etc.
!__________________________________________________________

!LIBRARY "MYFUNCTIONLIBRARY.lib"
CLEAR


DO Until Option = 9
   CLEAR
   CALL MENU
   INPUT Prompt "Select a choice from the menu.":Option

   SELECT CASE Option
   CASE IS = 1
        CALL SentenceBackwards
   CASE IS = 2
        CALL Encode
   CASE IS = 3
        CALL Decode
   CASE IS = 4
        CALL Acronym
   CASE IS = 5
        CALL CountWord
   CASE IS = 6
        CALL Pyramid
   CASE IS = 7
        CALL FirstLast
   CASE IS = 8
        CALL DoubleCheck
   CASE else
        PRINT
        PRINT "Unhandled exception refrenced at memory location: 0x011112110110"
   END SELECT


LOOP


END

!__________________________________________________________
!
! Menu
!__________________________________________________________
!
! This subprogram prints a menu for the user to select
! an option everytime they are prompted to do so
!__________________________________________________________

SUB Menu
    PRINT "________________________________"
    PRINT "| Menu                         |"
    PRINT "|______________________________|"
    PRINT "| 1. Print sentence backwards  |"
    PRINT "| 2. Encode a sentence         |"
    PRINT "| 3. Decode a sentence         |"
    PRINT "| 4. Make acronyms             |"
    PRINT "| 5. Count words in sentence   |"
    PRINT "| 6. Pyramid                   |"
    PRINT "| 7. First Shall be Last       |"
    PRINT "| 8. Double Check Inputs       |"
    PRINT "| 9. Quit                      |"
    PRINT "|______________________________|"
    PRINT
END SUB

!__________________________________________________________
!
! SentenceBackwards
!__________________________________________________________
!
! This subprogram lets the user input a sentence then prints
! the sentence they inputted backwards. It uses a fornext
! loop which reads the input backwards by using the
! paramter step -1
!__________________________________________________________
!
! Important Variables:
!
!  Sentence$ : The variable that stores the sentence that
!              will be printed backwards
!
!__________________________________________________________

SUB SentenceBackwards


    DO
       CLEAR
       IF ORD(Sentence$(1:1)) = 27 then
          EXIT DO
       END IF
       PRINT "Sentence Backwards Application"
       PRINT "____________________________________________"
       PRINT "Enter escape to Quit then Press Enter Twice."
       PRINT
       PRINT "____________________________________________"
       INPUT Prompt "Enter a sentence: ":Sentence$
       LET Length = Len(Sentence$)
       PRINT "____________________________________________"
       PRINT "Output:  ";
       FOR X = Length to 0 Step -1
           PRINT Sentence$(X:X);
       NEXT X
       PRINT
       PRINT "____________________________________________"
       PRINT "Press any key to continue."
       GET KEY PSE
       PRINT
    LOOP


END SUB

!__________________________________________________________
!
! Encodes a sentence
!__________________________________________________________
!
! This subprogram lets the user input a sentence then
! it encodes the sentence by changing the character they input
! two characters ahead in the alphabet, I.E. G = I
!__________________________________________________________
!
! Important Variables:
!
!  Sentence$ : The variable that stores the sentence that
!              will be encoded
!
!__________________________________________________________

SUB Encode


    DO
       CLEAR
       IF ORD(Sentence$(1:1)) = 27 then
          EXIT DO
       END IF
       PRINT "Encoding Application"
       PRINT "____________________________________________"
       PRINT "Enter escape to Quit then Press Enter Twice."
       INPUT Prompt "Enter a sentence to encode: ": Sentence$
       PRINT "____________________________________________"
       PRINT "Encoded Output: ";
       LET Length = Len(Sentence$)


       FOR X = 1 to Length
           IF Sentence$(X:X) <> " " and Sentence$(X:X) <> "y" and Sentence$(X:X) <> "Y" and Sentence$(X:X) <> "z" and Sentence$(X:X) <> "Z" then
              LET EncodedLetter = ORD(Sentence$(X:X)) + 2
              PRINT CHR$(EncodedLetter);
           END IF

           SELECT CASE Sentence$(X:X)
           CASE is = "Y"
                PRINT "A";
           CASE is = "Z"
                PRINT "B";
           CASE is = "y"
                PRINT "a";
           CASE is = "z"
                PRINT "b";
           CASE IS = " "
                PRINT " ";
           CASE else
           END SELECT


       NEXT X
       PRINT
       PRINT
       PRINT "Press any key to continue"
       GET KEY pse
    LOOP


END SUB

!__________________________________________________________
!
! Decode a sentence
!__________________________________________________________
!
! This subprogram lets the user input a sentence then
! it decodes the sentence by changing the character they input
! two characters behind in the alphabet, I.E. I = G
!__________________________________________________________
!
! Important Variables:
!
!  Sentence$ : The variable that stores the sentence that
!              will be decoded
!
!__________________________________________________________

SUB Decode


    DO
       CLEAR
       IF ORD(Sentence$(1:1)) = 27 then
          EXIT DO
       END IF
       PRINT "Decoding Application"
       PRINT "____________________________________________"
       PRINT "Enter escape to Quit then Press Enter Twice."
       INPUT Prompt "Enter a sentence to decode: ": Sentence$
       PRINT "____________________________________________"
       PRINT "Encoded Output: ";
       LET Length = Len(Sentence$)

       FOR X = 1 to Length
           IF Sentence$(X:X) <> " " and Sentence$(X:X) <> "y" and Sentence$(X:X) <> "Y" and Sentence$(X:X) <> "z" and Sentence$(X:X) <> "Z" then
              LET EncodedLetter = ORD(Sentence$(X:X)) - 2
              PRINT CHR$(EncodedLetter);
           END IF


           SELECT CASE (Sentence$(X:X))
           CASE is = "A"
                PRINT "Y";
           CASE is = "a"
                PRINT "y";
           CASE is = "B"
                PRINT "Z";
           CASE is = "b"
                PRINT "z";
           CASE is = " "
                PRINT " ";
           CASE else
           END SELECT


       NEXT X
       PRINT
       PRINT
       PRINT "Press any key to continue"
       GET KEY pse
    LOOP


END SUB

!__________________________________________________________
!
! Takes first letter of every word inputted to make acronym
!__________________________________________________________
!
! This subprogram lets the user input several words
! after the words are inputted the subprogram takes the first
! letter of every word capitalizes it and prints it
! to form an acronym
!__________________________________________________________
!
!  Sentence$ : The variable that stores the sentence that
!              will be made into an acronym
!__________________________________________________________


SUB Acronym


    DO
       CLEAR
       IF ORD(Sentence$(1:1)) = 27 then
          EXIT DO
       END IF
       PRINT "Acronym Application"
       PRINT "____________________________________________"
       PRINT "Enter escape to Quit then Press Enter Twice."
       PRINT
       PRINT "____________________________________________"
       INPUT Prompt "Enter a Sentence: ":Sentence$
       LET Length = Len(Sentence$)
       LET Sentence$ = Ucase$(Sentence$)
       PRINT "Acroynm: ";
       PRINT Sentence$(1:1);
       FOR X = 1 to Length
           IF Sentence$(X:X) = " " THEN
              PRINT Sentence$(X+1:X+1);
           END IF
       NEXT X
       PRINT
       PRINT "____________________________________________"
       PRINT "Press any key to continue."
       GET KEY pse
    LOOP


END SUB

!__________________________________________________________
!
! Count Words in a sentence
!__________________________________________________________
!
! This subprogram lets the user input a sentence then
! counts the number of words in the sentence
!__________________________________________________________
!
!  Sentence$ : The variable that stores the sentence that
!              will be checked to see how many words
!              it contains
!__________________________________________________________

SUB CountWord


    DO
       CLEAR
       IF ORD(Sentence$(1:1)) = 27 then
          EXIT DO
       END IF
       PRINT "Word Counting Application"
       PRINT "____________________________________________"
       PRINT "Enter escape to Quit then Press Enter Twice."
       PRINT
       PRINT "____________________________________________"
       INPUT Prompt "Enter a Sentence: ":Sentence$
       LET Length = Len(Sentence$)
       LET Sentence$ = Ucase$(Sentence$)
       FOR X = 1 to Length
           IF Sentence$(X:X) = " " and Sentence$(X+1:X+1) <> " " THEN
              LET TotalWords = TotalWords + 1
           END IF
       NEXT X
       PRINT USING "There are ### total words in the sentence":TotalWords + 1
       PRINT "____________________________________________"
       PRINT "Press any key to continue."
       LET TotalWords = 0
       GET KEY pse
    LOOP


END SUB

!__________________________________________________________
!
! Prints name as a pyramid
!__________________________________________________________
!
! This subprogram tells the user to input their age and 
! their name. For every year they are old the program
! will print a smiley denoted by ":)". Then the program
! will take their name invoke the function pyramid and
! print their name in a pyramid structure, for example
! if the name inputted was Bill the program would print
!
! B
! Bi
! Bil
! Bill
!__________________________________________________________
!
! Important Variables:
!
! Name$ = The user inputted name, it is printed in a pyramid
!         like format.
!
! Age   = The user inputted age, for every year that the person
!         using the program has existed a smiley is printed 
!         as such :)
!__________________________________________________________

SUB Pyramid

    !    DECLARE FUNCTION Center
    !
    !
    !    DO
    !       CLEAR
    !       IF ORD(Sentence$(1:1)) = 27 then
    !          EXIT DO
    !       END IF
    !       PRINT "Pyramid Application"
    !       PRINT "____________________________________________"
    !       PRINT "Enter escape to Quit then Press Enter Twice."
    !       PRINT
    !
    !       INPUT Prompt "Enter your name: ":Name$
    !       INPUT Prompt "Enter your age: ":Age
    !
    !       FOR X = 1 to Age
    !           PRINT ":)"
    !       NEXT X
    !
    !
    !       LET PrintArea   =Center(Name$)
    !       PRINT Tab(PrintArea); Name$
    !       GET KEY pse
    !
    !       PRINT
    !       PRINT
    !       PRINT
    !       PRINT
    !
    !       FOR X = 1 to len(Name$)
    !           LET C =  C + 1
    !           PRINT Name$(X:X)
    !           FOR Q = 1 to C
    !               PRINT Name$(Q:Q);
    !           NEXT Q
    !       NEXT x
    !
    !    LOOP

END SUB

!__________________________________________________________
!
! First shall be last
!__________________________________________________________
!
! This program lets the user input their first name and 
! last name into one variable then it prints their last name
! then first name
!__________________________________________________________

SUB FirstLast

    DO
       CLEAR
       IF ORD(Sentence$(1:1)) = 27  then
          EXIT DO
       END IF
       PRINT "First Shall be Last Application"
       PRINT "____________________________________________"
       PRINT "Enter escape to Quit then Press Enter Twice."

       INPUT Prompt "Enter your first then last name: ":Sentence$
       PRINT "____________________________________________"
       PRINT "Output: ";
       LET Length = Len(Sentence$)

       FOR X = Length to 0 step -1
           IF Sentence$(X:X) = " " or Sentence$(X:X) = "," and Flag <> 1 then
              LET LastName = X
              LET Flag = 1
           END IF
       NEXT x

       FOR Y = Lastname to Length
           PRINT trim$(Sentence$(Y:Y));
       NEXT Y
       PRINT ", ";

       FOR Z = 0 to Lastname
           PRINT trim$(Sentence$(Z:Z));
       NEXT Z

       PRINT
       PRINT "Press any key to continue."
       GET KEY pse
    LOOP

END SUB

!__________________________________________________________
!
! Double check inputs made by user
!__________________________________________________________
!
! This program lets the user input their age and name
! and verifies if they entered a numerical value for their
! age or a string value for their name, if they did not
! they are given an error message and must renter their
! name or age.
!__________________________________________________________

SUB DoubleCheck


    DO
       CLEAR
       INPUT Prompt "Enter Escape to Exit Application, or enter any other value to run application.":CheckRun$
       IF ORD(CheckRun$(1:1)) = 27 then
          EXIT DO
       END IF

       CLEAR

       PRINT "Double Check Application"
       PRINT "____________________________________________"

       DO
          INPUT Prompt "Enter your name: ":Name$
          LET Length = Len(Name$)
          LET NameCheck$ = UCASE$(Name$)
          LET AlphabeticalCheckFlag = 0

          FOR X = 1 to Length
              IF NameCheck$(X:X) < "A" OR NameCheck$(X:X) > "Z" THEN
                 PRINT "Your name should only contain alphabetical data."
                 LET AlphabeticalCheckFlag = 1
                 EXIT FOR
              END IF
          NEXT x

          IF AlphabeticalCheckFlag = 0 THEN
             PRINT USING "Your name is ###################": Name$
             EXIT DO
          END IF
       LOOP


       DO
          INPUT Prompt "Enter your age: ":Age$
          LET Length = Len(Age$)
          LET NumericalCheckFlag = 0

          FOR X = 1 to Length
              IF ORD(Age$(X:X)) < 1 OR ORD(Age$(X:X)) > 100 THEN
                 PRINT "Your age should contain numerical data."
                 LET NumericalCheckFlag = 1
                 EXIT FOR
              END IF
          NEXT x

          IF NumericalCheckFlag = 0 THEN
             PRINT USING "You are #### years old.": Age$
             EXIT DO
          END IF
       LOOP












       PRINT "____________________________________________"
       PRINT "Press any key to continue."
       GET KEY PSE
       PRINT
    LOOP


END SUB
